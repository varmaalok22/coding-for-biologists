# -*- coding: utf-8 -*-
"""
Created on Fri Jul 05 13:38:52 2019

@author: Aalok
"""

# Text mining Shakespeare's plays

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import re # Regular expressions. Very useful.

# Step 0 - Load the text into a variable
directory = r'D:\Coding for Biologists\coding-for-biologists\Shakespeare'
filename = r'All Plays.txt'
filepath = os.path.join(directory, filename)

with open(filepath, 'r') as f:
    #all_plays = f.read().decode('utf-8')
    all_plays = f.read()
    
# Check that everything has loaded correctly
# The 37 plays are separated by a string of ----------
len(all_plays.split('----------'))

# Step 1 - Preprocessing the text: Text Normalization
# A. Convert all the text into lowercase.
all_plays = all_plays.lower()

# B. Remove numbers
print("Before number removal: %d" %(len(all_plays)))
all_plays = re.sub(r'\d+', '', all_plays)
print("After number removal: %d" %(len(all_plays)))

# C. Remove some punctuation (commas, brackets, fullstops, question and exclamation marks)
print("Before punctuation removal: %d" %(len(all_plays)))
all_plays = re.sub('[?!,:;_\[\{\]\}\(\)"&.]+', '', all_plays) # Replace punctuation with nothing.
all_plays = re.sub('[\n]+', ' ', all_plays)

# Remove all Unicode stuff, to make the entire string contain only ASCII characters.
all_plays = re.sub('[^\x00-\x7F]+', ' ', all_plays)
all_plays = " ".join(all_plays.split()) # Get rid of excessive white space
print("After punctuation removal: %d" %(len(all_plays)))

# Replace the single quotation (but not apostrophe) with a double quote
all_plays = re.sub('\'(\w*)\'', r'"\1"', all_plays) # This needs improvement.

# TOKENIZATION - i.e. identifying words
unique_words = np.unique(all_plays.split())

# Remove stop words - "a", "an", "the" and such. Analysis can be done with and without these, for comparison.
