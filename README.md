# Coding for Biologists

## General information

NCBS - July 2019.

Aim: A biologist-friendly introduction to coding, independent of programming language.

Each class roughly 1 hour long. Group size approx. 10 people.

## Agenda
Check out [this document](https://docs.google.com/document/d/1W-O5-BGjBKlwmnM60PrVxhVUTj-STMlH14uHJ5bIan8/edit?usp=sharing) for the agenda.