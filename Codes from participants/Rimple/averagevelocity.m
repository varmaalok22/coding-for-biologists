% directory
directory = 'C:\Users\admin\Desktop\coding';
data = readtable(fullfile(directory, 'Control_Pooled.xlsx'));
mainarray = data(:,:);
y = unique(data.Worm_ID);
% wormid = 3; % enter the number you want
% currentwormid = y{wormid, 1};
no_of_worms = length(y);
avgvel = zeros(1,no_of_worms);
% total_dist = 0;
totdist = zeros(1, no_of_worms);
distance = zeros(no_of_worms,810);
total_dist = zeros(no_of_worms,810);
velocity = zeros(no_of_worms,801);
average_velocity = zeros(no_of_worms,1);
worms = 1:20
% all_readings = cell(2,no_of_worms);

for i=1:no_of_worms
% distance counter
    
    subarray = data(strcmp(mainarray.Worm_ID, y{i}),:);
    
    for j = 1:length(subarray.Frame_No)-1
        x1 = subarray.Centroid_x(j);
        y1 = subarray.Centroid_y(j);
        x2 = subarray.Centroid_x(j+1);
        y2 = subarray.Centroid_y(j+1);
% distance calculation
        distance(i, j) = sqrt(((x2-x1)^2)+((y2-y1)^2));
        total_dist(i, j) = sum(distance(i,:));
% calculate velocity
        velocity(i,j) = distance(i,j)/0.15;

%calculate instantaeneous velocity
        
% calculate average velocity
    end
%     totdist(1,i) = total_dist(i,j)
    average_velocity(i,1) = total_dist(i,j)/120;
%         avgvel(1,i) = average_velocity;
end

disp(average_velocity)
figure
boxplot(worms, average_velocity)% plotting didnt  work


%% homework
% look for timer functions. You start a time before start of code and stops
% at the end to see how fast the code is
