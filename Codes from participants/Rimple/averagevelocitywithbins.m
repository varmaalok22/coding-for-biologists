%%average velocity with bins
directory = 'C:\Users\admin\Desktop\coding';
data = readtable(fullfile(directory, 'Treated_Pooled.xlsx'));
mainarray = data(:,:);
y = unique(data.Worm_ID);
% wormid = 3; % enter the number you want
% currentwormid = y{wormid, 1};
no_of_worms = length(y);
b = 10; % no of bins
binned_data = zeros(no_of_worms, b);
insvel = zeros(no_of_worms, 800);
avgvel = zeros(1,no_of_worms);
normalised = zeros(no_of_worms, b);
binnedaverage = zeros(1, b);
% totdist = zeros(1, no_of_worms);
distance = zeros(no_of_worms,800);
% total_dist = zeros(no_of_worms,810);
% velocity = zeros(no_of_worms,801);
% average_velocity = zeros(no_of_worms,1);
%worms = 1:20;
for i=1:no_of_worms
% distance counter
    
    subarray = data(strcmp(mainarray.Worm_ID, y{i}),:);
    
    x1 = subarray.Centroid_x(2:end);
    x2 = subarray.Centroid_x(1:end-1);
    y1 = subarray.Centroid_y(2:end);
    y2 = subarray.Centroid_y(1:end-1);
    
    distance(i,1:length(x1)) = sqrt((x2-x1).^2+(y2-y1).^2 ); 
    %(x2-x1)^2 will be a matrix multiplication
    insvel(i,1:length(x1)) = distance(i,1:length(x1))/0.15;

end
for i=1:no_of_worms
    subarray = data(strcmp(mainarray.Worm_ID, y{i}),:);
%     for j=1: subarray.Frame_No(end); 
         bin = floor(subarray.Frame_No(end)/b);
         
            for k=1:b % to count bins
            binned_data(i,k)= sum(distance(i,((k-1)*bin+1:k*bin)))/bin;
            
                
            end
                minimum = min(binned_data(i,:));
                maximum = max(binned_data(i,:));
            for f=1:b
                   normalised(i,f)= (binned_data(i,f)-minimum)/(maximum-minimum);
                   
            end
            
end
for f=1:b
    binnedaverage(1,f)= (sum(normalised(1:no_of_worms,f)))/no_of_worms;
end

    
% distance 
% plot(1:800,distance)
% plot(1:800,insvel)
% plot(1:20,avgvel)
% plot(1:20,binned_data)
figure
plot(binnedaverage)