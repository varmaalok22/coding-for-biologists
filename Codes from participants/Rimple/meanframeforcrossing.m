%mean time for crossing
%using logical or bolean to store true false.
directory = 'C:\Users\admin\Desktop\coding';
data = readtable(fullfile(directory, 'Control_Pooled.xlsx'));

Threshold = 0.75*627;
no_of_worms = 20;
counter=0;
all_readings = nan(1,no_of_worms);%zeros means an array of zero is made ; ones(size) and nan(size)
%percentage_of_worms = 0;
for j= 1:no_of_worms
    start_index=(1+(j-1)*801);
    end_index=(j*801);
    for i=(start_index:end_index)
        cen_x = data.Centroid_x(i);
        %all_readings(1,j) = j;
            if cen_x > Threshold
            all_readings(1,j) = data.Frame_No(i);
            counter=counter+1;
            break;
            end
            
            %if i == end_index;
            %all_readings{2,j} = 0
            %end 
    end
end    
all_readings
%sum(all_readings, 2)
%nanmean(all_readings) %nan mean will ignore all nans for mean
sprintf ('mean time to cross = %f', nanmean(all_readings))

    %percentage_of_worms = counter/no_of_worms*100;
    %fprintf('%d of worms crossed \n', percentage_of_worms) %\n moved the line to the next line this is just beautification
    %sprintf ('%d of worms crossed', percentage_of_worms)  % %f is for decimal, %d is for integer
  % sum(arrayname, dimension) Eg:sum(all_readings, 1); sum is function;
  % arrayname and dimensions are called arguments
  % within arguments, there are keyword arguments or kwargs
  
  
  %logical indexing using frames[frames!=0] where [####00#0] will give you
  %[#####] and mean(frames) will give you mean of only those numbers kept.

 %another solution is initialise as Nans and then modify nans where
 %relevant an then use nanmean or equivalent to ignore all nans
 
 %another way potato = [] and if worm crosses, potato = [potato {#,w#}]
 %else nothing. 