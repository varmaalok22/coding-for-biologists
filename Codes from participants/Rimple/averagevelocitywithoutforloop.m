directory = 'C:\Users\admin\Desktop\coding';
data = readtable(fullfile(directory, 'Control_Pooled.xlsx'));
mainarray = data(:,:);
y = unique(data.Worm_ID);
% wormid = 3; % enter the number you want
% currentwormid = y{wormid, 1};
no_of_worms = length(y);
insvel = zeros(no_of_worms, 800);
avgvel = zeros(1,no_of_worms);
% totdist = zeros(1, no_of_worms);
distance = zeros(no_of_worms,800);
% total_dist = zeros(no_of_worms,810);
% velocity = zeros(no_of_worms,801);
% average_velocity = zeros(no_of_worms,1);
worms = 1:20;
for i=1:no_of_worms
% distance counter
    
    subarray = data(strcmp(mainarray.Worm_ID, y{i}),:);
    
    x1 = subarray.Centroid_x(2:end);
    x2 = subarray.Centroid_x(1:end-1);
    y1 = subarray.Centroid_y(2:end);
    y2 = subarray.Centroid_y(1:end-1);
    
    distance(i,:) = sqrt((x2-x1).^2+(y2-y1).^2 ); 
    %(x2-x1)^2 will be a matrix multiplication
    insvel(i,:) = distance(1,:)/0.15;
    avgvel(1,i) = sum(distance(i,:)/(800*0.15));
end

% distance 
plot(1:800,distance)
plot(1:800,insvel)
plot(1:20,avgvel)



